class HomeController < ApplicationController
    # before_action :authenticate_user!
    def index
      @movies = Movie.all
      @movies_watched = Movie.most_watched
    end

    def history
    end
end
