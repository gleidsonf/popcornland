class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def is_admin?
    unless current_user.admin?
      flash[:danger] = "Você não tem acesso de administrador"
      redirect_to root_path
    end
  end

end
