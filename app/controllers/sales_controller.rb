class SalesController < ApplicationController

  def create
    # redirect_to root_path
    @sale = Sale.new sale_params
    if @sale.save
      redirect_to "#{request.base_url}/payment/#{@sale.id}"
      # redirect_to payments_path(sale_id: @sale.)
    else
      redirect_to session_path(@sale.session), alert: "Verifique os erros"
      # render :show, :session_id => @sale.session.id alert: "Operação não concluída"
    end
  end

  private

  def sale_params
    params.require(:sale).permit(:user, :user_id, :session, :session_id, seats: [], seat_ids: [])
  end
end
