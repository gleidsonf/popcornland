class PaymentsController < ApplicationController
  before_action :authenticate_user!

  def payment
    @payment = Payment.new
    @sale = Sale.find(params[:sale_id])
    @payment.ticket_type = Array.new(@sale.seats.count)
  end

  def create
    @payment = Payment.new payment_params
    @payment.total = @payment.total / @payment.parcels if @payment.total >= 30
    if @payment.save
      TicketMailer.sample_email(@payment).deliver_now
      redirect_to @payment, notice: "Pagamento confirmado!"
    else
      render :payment, alert: "Ocorreu um erro!"
    end
  end

  def show
    @payment = Payment.find(params[:id])
    @movie = @payment.sale.session.movie
  end

  private
  def payment_params
    params.require(:payment).permit(:total, :sale_id, :parcels, :payment_method)
  end

end
