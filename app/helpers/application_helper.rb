module ApplicationHelper
  def link_to(text, path, options={})
    super(text, path, options) unless options[:admin] and !current_user.nil? and !current_user.admin?
  end

  def button_to(text, path, options={})
    super(text, path, options) unless options[:admin] and !current_user.nil? and !current_user.admin?
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
