ActiveAdmin.register Ticket do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :value, :half_value, :session_id

form title: 'Novo ingresso' do |f|
  inputs 'Detalhes' do
    input :value
    input :half_value
    f.input :session_id, :label => 'Sessão', :as => :select, :collection => Session.all.map{|s| ["#{s.movie.title} - #{s.start_time.strftime('%H:%M')}h", s.id]}
    # li "Created at #{f.object.created_at}" unless f.object.new_record?
  end
  # inputs 'Content', :body
  # para "Press cancel to return to the list without saving."
  actions
end

end
