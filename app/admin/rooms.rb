ActiveAdmin.register Room do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :number

form title: 'Nova sala' do |f|
  inputs 'Detalhes' do
    input :number
  end
  actions
end

end
