ActiveAdmin.register Seat do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :row, :armchair

  form title: 'Novo assento' do |f|
    inputs 'Detalhes' do
      input :row
      input :armchair
    end
    actions
  end

end
