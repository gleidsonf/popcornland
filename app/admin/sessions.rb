ActiveAdmin.register Session do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :movie_id, :cinema_id, :start_time, :room_id

  form title: 'Nova sessão' do |f|
    inputs 'Detalhes' do
      f.input :movie_id, :label => 'Filme', :as => :select, :collection => Movie.all.map{|s| ["#{s.title}", s.id]}
      f.input :cinema_id, :label => 'Cinema', :as => :select, :collection => Cinema.all.map{|s| ["#{s.name}", s.id]}
      f.input :room_id, :label => 'Sala', :as => :select, :collection => Room.all.map{|s| ["#{s.number}", s.id]}
      input :start_time
    end
    # inputs 'Content', :body
    # para "Press cancel to return to the list without saving."
    actions
  end

  before_create do |session|
    session.seats = Seat.all
  end
end
