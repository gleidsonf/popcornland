ActiveAdmin.register Cinema do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :address
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form title: 'Novo cinema' do |f|
  inputs 'Detalhes' do
    input :name
    input :address
  end
  actions
end
# https://maps.google.com.br/maps?q=Avenida+Castelo+Branco,+400,+Fortaleza,+CE&

end
