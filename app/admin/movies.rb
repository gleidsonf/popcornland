ActiveAdmin.register Movie do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :title, :genre, :age_rating

  form title: 'Novo Filme' do |f|
    inputs 'Detalhes' do
      input :title
      input :genre
      input :age_rating
    end
    actions
  end

end
