class TicketMailer < ApplicationMailer
  default from: "teste.acens@gmail.com"

  def sample_email(payment)
    @payment = payment
    @movie = payment.sale.session.movie
    mail(to: "teste.acens@gmail.com", subject: "Seus ingresos para #{@movie.title}")
  end
end
