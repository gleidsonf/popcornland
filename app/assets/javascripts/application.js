// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs

//= require jquery3
//= require twitter/bootstrap
// require jquery-3.2.1.min
// require bootstrap.min
//= require bootstrap-notify
//= require jquery.ajaxchimp
//= require jquery.magnific-popup.min
//= require jquery.mmenu
//= require jquery.inview.min
//= require jquery.countTo.min
//= require jquery.countdown.min
//= require owl.carousel.min
//= require imagesloaded.pkgd.min
//= require isotope.pkgd.min
//= require headroom
//= require custom

//= require revolution/js/jquery.themepunch.tools.min
//= require revolution/js/jquery.themepunch.revolution.min

//= require revolution/js/extensions/revolution.extension.actions.min
//= require revolution/js/extensions/revolution.extension.carousel.min
//= require revolution/js/extensions/revolution.extension.kenburn.min
//= require revolution/js/extensions/revolution.extension.layeranimation.min
//= require revolution/js/extensions/revolution.extension.migration.min
//= require revolution/js/extensions/revolution.extension.navigation.min
//= require revolution/js/extensions/revolution.extension.parallax.min
//= require revolution/js/extensions/revolution.extension.slideanims.min
//= require revolution/js/extensions/revolution.extension.video.min



$(document).on('turbolinks:load', function() {
  // $(window).on('load', function () {
      var loading = $('.loading');
      loading.delay(1000).fadeOut(1000);
  // }); // end of window load function
});
