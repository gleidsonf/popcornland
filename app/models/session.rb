class Session < ApplicationRecord
  belongs_to :movie
  belongs_to :cinema
  has_many :session_seats
  has_many :seats, through: :session_seats
  has_one :ticket
  has_many :sales
  has_one :room
  def start_time_br
    self.start_time.strftime("%H:%M")
  end
end
