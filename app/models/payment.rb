class Payment < ApplicationRecord
  validates_uniqueness_of :sale_id

  belongs_to :sale
  enum payment_method: [:debit, :credit, :cash]

  after_initialize :set_default_payment_method, :if => :new_record?

  def set_default_payment_method
    self.payment_method ||= :cash
  end

end
