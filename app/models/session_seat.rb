class SessionSeat < ApplicationRecord
  belongs_to :seat
  belongs_to :session
end
