class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: [:client, :admin, :student]

  after_initialize :set_default_role, :if => :new_record?
  has_many :sales
  def set_default_role
    self.role ||= :client
  end

end
