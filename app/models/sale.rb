class Sale < ApplicationRecord
  belongs_to :user
  belongs_to :session

  has_many :sale_seats
  has_many :seats, through: :sale_seats

  has_one :payment
  # has_many :payments
  validate :validate_seats_length
  # validates_length_of :seats, ]

  def validate_seats_length
    if self.seat_ids.size > 4
      self.errors.add(:seat_ids, "Número máximo de ingressos é 4")
    end
  end
end
