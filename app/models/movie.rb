class Movie < ApplicationRecord
  has_many :sessions

  scope :most_watched, -> { all.sort_by {|movie| movie.highest }.reverse }

  def highest
    self.sessions.map {|session| session.sales.count}.sum
  end

end
