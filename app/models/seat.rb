class Seat < ApplicationRecord

  has_many :session_seats
  has_many :sessions, through: :session_seats

  has_many :sale_seats
  has_many :sales, through: :sale_seats

  def name
    "#{self.row}#{self.armchair}"
  end
end
