json.extract! ticket, :id, :value, :half_value, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
