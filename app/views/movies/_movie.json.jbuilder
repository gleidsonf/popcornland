json.extract! movie, :id, :title, :genre, :age_rating, :created_at, :updated_at
json.url movie_url(movie, format: :json)
