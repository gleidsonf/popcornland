json.extract! session, :id, :movie_id, :cinema_id, :start_time, :created_at, :updated_at
json.url session_url(session, format: :json)
