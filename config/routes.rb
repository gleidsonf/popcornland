Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :sessions, only: [:index, :show]
  resources :cinemas, only: [:index, :show]
  resources :sales

  resources :payments, only: [:create, :show]

  post "/payments/refresh", to: "payments#refresh_value"
  get "/payment/:sale_id", to: "payments#payment", as: :payment_path
  get "/history", to: "home#history", as: :history

  #devise_for :users
  root "home#index"

  resources :movies, only: [:index, :show]
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  devise_scope :user do
    match '/sessions/user', to: 'devise/sessions#create', via: :post
  end
end
