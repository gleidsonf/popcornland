class CreateSessionSeats < ActiveRecord::Migration[5.1]
  def change
    create_table :session_seats do |t|
      t.references :seat, foreign_key: true
      t.references :session, foreign_key: true

      t.timestamps
    end
  end
end
