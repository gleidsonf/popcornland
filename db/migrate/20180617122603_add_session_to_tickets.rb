class AddSessionToTickets < ActiveRecord::Migration[5.1]
  def change
    add_reference :tickets, :session, foreign_key: true
  end
end
