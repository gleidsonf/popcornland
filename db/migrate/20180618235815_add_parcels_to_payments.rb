class AddParcelsToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :parcels, :integer
  end
end
