class AddRoomToSessions < ActiveRecord::Migration[5.1]
  def change
    add_reference :sessions, :room, foreign_key: true
  end
end
