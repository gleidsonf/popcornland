class CreateSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :sessions do |t|
      t.references :movie, foreign_key: true
      t.references :cinema, foreign_key: true
      t.time :start_time

      t.timestamps
    end
  end
end
