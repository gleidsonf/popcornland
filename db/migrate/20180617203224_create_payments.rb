class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :sale, foreign_key: true
      t.string :payment_method
      t.decimal :total, precision: 5, scale: 2

      t.timestamps
    end
  end
end
