class CreateSaleSeats < ActiveRecord::Migration[5.1]
  def change
    create_table :sale_seats do |t|
      t.references :sale, foreign_key: true
      t.references :seat, foreign_key: true

      t.timestamps
    end
  end
end
