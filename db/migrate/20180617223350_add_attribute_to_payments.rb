class AddAttributeToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :ticket_type, :string, array: true, default: []
  end
end
