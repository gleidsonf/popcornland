class CreateSeats < ActiveRecord::Migration[5.1]
  def change
    create_table :seats do |t|
      t.string :row
      t.integer :armchair

      t.timestamps
    end
  end
end
